import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany, Generated } from 'typeorm';

@Entity('notification')
export class Notification {
    @PrimaryGeneratedColumn({ type: 'integer' })
    public id: number;
    @Column('text')
    public type: string;
    @Column('text')
    public title: string;
    @Column({ type: 'nvarchar'})
    public text: string;
    @Column('integer')
    public expires: number;
    @Column('text')
    public requirement: string;
    @Column({ type: 'text'})
    public link: string;
    @Column({ type: 'text'})
    public image: string;
}
