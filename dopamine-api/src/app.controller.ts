import {
  Controller, Post, Get, Delete, Patch, Query, Body, Param, Put, Req, BadRequestException,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('')
  public async allNotifications(){
    return await this.appService.getAll()
  }
  @Post('')
  public async createNotification(@Body() notification){
    return await this.appService.createNotification(notification);
  }
  @Delete('')
  public async deleteNotification(@Body() id){
    return await this.appService.removeNotification(id);
  }
  @Put('')
  public async updateNotification(@Body() body){
    return await this.appService.updateNotification(body)
  }
}
