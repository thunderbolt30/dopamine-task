import { createConnection } from 'typeorm';
import { Notification } from '../database/entities/notification.entity';


const main = async ()=>{
    const connection = await createConnection();
    const notificationRepo = connection.getRepository(Notification);
    await notificationRepo.delete({})
    await connection.close();
    console.log('Data removed from database!');


}
main()
.catch(console.log);
