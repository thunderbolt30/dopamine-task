import { createConnection } from 'typeorm';
import { Notification } from '../database/entities/notification.entity';

const main = async () => {
    const connection = await createConnection();
    const notificationRepo = connection.getRepository(Notification);
    // creating notification

    let notification1 = new Notification()
    notification1.expires = 5000;
    notification1.id = 3333
    notification1.text = ' new notification';
    notification1.title = 'testing';
    notification1.type = 'text';
    notification1.requirement= 'Deposit $50 to win',
    notification1.link = 'https://www.google.com/',
    notification1.image =  'https://www.freeiconspng.com/uploads/leistungen-promotion-icon-png-0.png',
    notification1 = await notificationRepo.save(notification1),

    await connection.close();
    console.log(`Data seeded successfully`);
};
main()
    .catch(console.log);
