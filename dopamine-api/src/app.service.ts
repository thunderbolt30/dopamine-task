
import { Repository } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Notification } from './database/entities/notification.entity';
@Injectable()
export class AppService {
  public constructor(
    @InjectRepository(Notification) private readonly notificationRepo: Repository<Notification>) {

  }

  public async getAll() {
    const foundNotifications = await this.notificationRepo.find();
    return foundNotifications;
  }
  public async createNotification(notification) {
    let notificationToCreate = this.notificationRepo.create();
    notificationToCreate = notification;
    return await this.notificationRepo.save(notificationToCreate);

  }
  public async  removeNotification(idToRemove) {

    const foundNotification = await this.notificationRepo.findOne({ where: { id: idToRemove.id } });

    return await this.notificationRepo.delete(foundNotification);
  }
  public async updateNotification(body) {

    const foundNotification = await this.notificationRepo.findOne({ where: { id: body.id } });
    await this.notificationRepo.delete(foundNotification);
    const createNotification = this.notificationRepo.create();
    createNotification.id = body.id;
    createNotification.expires = body.expires;
    createNotification.image = body.image;
    createNotification.link = body.link;
    createNotification.requirement = body.requirement;
    createNotification.text = body.text;
    createNotification.title = body.title;
    createNotification.type = body.type;
    return await this.notificationRepo.save(createNotification);
  }
}
