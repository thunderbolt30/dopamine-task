import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Notification } from './database/entities/notification.entity';
import { DatabaseModule } from './database/entities/database.module';

@Module({
  imports: [TypeOrmModule.forFeature([Notification]),DatabaseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
