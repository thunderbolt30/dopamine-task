import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotificationDTO } from '../dto/notification-dto';

@Component({
  selector: 'app-notification-info',
  templateUrl: './notification-info.component.html',
  styleUrls: ['./notification-info.component.css']
})
export class NotificationInfoComponent implements OnInit {
  public notificationToShow: NotificationDTO;

  constructor() { }
  @Input() public set notification(value: NotificationDTO) {
    this.notificationToShow = value;
  }
  @Output() removeEmit = new EventEmitter<any>();

  ngOnInit(): void {
    const notification = this.notificationToShow;
    const remove = this.removeEmit;
    const removeAfter = this.notificationToShow.expires;

    if(notification.expires){
      setTimeout(() => {
        remove.emit(notification.id);
  
      }, notification.expires);
    }
  }
}
