export class NotificationDTO {
    public id: number;
    public type?: string;
    public title: string;
    public text: string;
    public expires: number;
    public requirement?: string;
    public link?: string;
    public image?: string;
}