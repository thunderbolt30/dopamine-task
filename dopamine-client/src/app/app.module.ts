import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MatMenuModule} from '@angular/material/menu';
import {NotificationInfoComponent} from './notification-info/notification-info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatCardModule} from '@angular/material/card';
import { NotificationService } from './core/services/notification.service';
import { CoreModule } from './core/core.module';


@NgModule({
  declarations: [
    AppComponent,
    NotificationInfoComponent,
  ],
  imports: [
    BrowserModule,
    MatMenuModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatBadgeModule,
    MatCardModule,
    CoreModule
  ],
  providers: [NotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
