import { Component, OnInit, OnDestroy } from '@angular/core';

import { NotificationDTO } from './dto/notification-dto';
import { NotificationService } from './core/services/notification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  private updateSubscription: Subscription;
  private notificationAllSubScription: Subscription;
  private addNotification: Subscription;
  public notificationAll: NotificationDTO[] = [];
  public notificationCount: number;
  public constructor(private readonly notificationService: NotificationService) {
  }
  ngOnInit() {
    this.notificationAllSubScription = this.notificationService.getAllNotifications().subscribe((data) => {
      this.notificationAll = data,
        this.notificationCount = this.notificationAll.filter(x => x.type !== 'bonus').length;
    });
  }
  public onClickUpdate() {
    const notificationToUpdate = {id: 9999,

      type: 'text',

      title: 'Test update',

      text: 'Test text notification',

      expires: 5000,
      requirement: 'none',
      link: 'https://www.google.com/',
      image: 'https://www.freeiconspng.com/uploads/leistungen-promotion-icon-png-0.png'
    };
    this.updateSubscription = this.notificationService.updateNotification(notificationToUpdate).subscribe(
      (data) => {
        this.notificationAll = this.notificationAll.filter(x => x.id !== notificationToUpdate.id),
        this.notificationAll.push(data),
        this.notificationCount++;
      }
    );

  }
  public removeNotificationAfterExpiration(id: number): void {
    this.notificationAll = this.notificationAll.filter(x => x.id !== id);
    this.notificationCount = this.notificationAll.filter(x => x.type !== 'bonus').length;
  }
  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
    this.notificationAllSubScription.unsubscribe();
    this.addNotification.unsubscribe();

  }
  onClick(): void {
    const newNotification = {
      id: 1111,

      type: 'text',

      title: 'Test notification',

      text: 'Test text notification',

      expires: 5000,
      requirement: 'none',
      link: 'https://www.google.com/',
      image: 'https://www.freeiconspng.com/uploads/leistungen-promotion-icon-png-0.png'
    };
    this.addNotification =
    this.notificationService.createNewNotification(newNotification).subscribe(
      () => {
        this.notificationAll.push(newNotification),
      this.notificationCount++; }
    );

  }

}
