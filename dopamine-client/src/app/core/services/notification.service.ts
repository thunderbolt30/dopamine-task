import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotificationDTO } from '../../dto/notification-dto';
import { Injectable } from '@angular/core';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
@Injectable({
    providedIn: 'root'
  })
export class NotificationService {
    public constructor( private readonly http: HttpClient) {

    }
    public getAllNotifications(): Observable<any> {
        return this.http.get<any>('http://localhost:3000');
    }
    public createNewNotification(body): Observable<any> {
        return this.http.post<any>('http://localhost:3000', body);
    }
    public updateNotification(body): Observable<any> {
        return this.http.put<any>('http://localhost:3000', body );
    }
}
