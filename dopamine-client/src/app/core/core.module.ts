import { NotificationService } from './services/notification.service';
import { HttpClientModule } from '@angular/common/http';
import { Optional, SkipSelf, NgModule } from '@angular/core';

@NgModule({
    providers: [NotificationService, ],
    declarations: [],
    imports: [
    HttpClientModule
    ],
    exports: [HttpClientModule]
  })
  export class CoreModule {
    constructor(@Optional() @SkipSelf() parent: CoreModule) {
      if (parent) {
        throw new Error(`CoreModule has already been initialized!`);
      }
    }
  }
  